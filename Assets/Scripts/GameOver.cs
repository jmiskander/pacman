using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public int gameScore = 0;
    public Text ScoreText;
    public Text ShadowScore;
    public void QuitGame()
    {
        Debug.Log("QUIT !");
        Application.Quit();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenuGame()
    {
        PlayerPrefs.SetInt("highScore", gameScore);
        SceneManager.LoadScene("Menu");
    }
    // Start is called before the first frame update
    void Start()
    {
        gameScore = 0;
    }

    // Update is called once per frame
    void Update()
    {
        ScoreText.text = "Score : " + gameScore;
        ShadowScore.text = "Score : " + gameScore;
        
    }
}
