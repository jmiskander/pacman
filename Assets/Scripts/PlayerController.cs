using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    private NavMeshAgent player;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vertic = Input.GetAxis("Vertical");

        //Debug.Log(horiz + " / " + vertic);


        if (Mathf.Abs(vertic) > 0.1 || Mathf.Abs(horiz) > 0.1)
        {
            rb.velocity = new Vector3(horiz, 0, vertic) * 100f * Time.deltaTime;
        }
        else // stop when not clicking
        {
            rb.velocity = Vector3.zero;

        }
    }


}
