using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesManager : MonoBehaviour
{

    static int lives = 3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void setLives(int value)
    {
        lives -= value;
    }

    public static int getLives()
    {
        return lives;
    }

    void OnGUI()
    {
       // GUILayout.Label("Score: " + score.ToString());
       // GUILayout.Label("Lives: " + lives.ToString());
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Node")
        {
            //Destroy(other.gameObject);
            //Debug.Log("Collision !");

            //setScore(10);
            setLives(1);
        }

    }
}
