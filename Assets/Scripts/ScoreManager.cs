using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    static int score = 0;


    public static int health;

    // Start is called before the first frame update
    void Start()
    {
        //health = 3;
        //heart1.gameObject.SetActive(true);
        //heart2.gameObject.SetActive(true);
        //heart3.gameObject.SetActive(true);

    }
    // Update is called once per frame
    void Update()
    {
    }
    public static void setScore(int value)
    {
        score += value;
    }
    public static int getScore()
    {
        return score;
    }
    void OnGUI()
    {
        GUILayout.Label("                                                                             " + score.ToString());
       
        //GUILayout.Label("Lives: " + lives.ToString());
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Node")
        {
            setScore(10);
            //setLives(1);
        }

    }
}
