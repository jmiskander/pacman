using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GhostMovement : MonoBehaviour
{

    public NavMeshAgent enemy;
    //public float MoveSpeed = 1f;



    public Transform player;

    // Start is called before the first frame update
    void Start()
    {
        enemy = GetComponent<NavMeshAgent>();
        //enemy.speed = 2f;
    }

    // Update is called once per frame
    void Update()
    {
        enemy.SetDestination(player.position) ;
        //transform.position += transform.forward * MoveSpeed * Time.deltaTime;
    }

    
}
